// a_i=a_i-1+a_i-2+a_i-3+a_i-4+a_i-5+a_i-6
struct Matrix {
    ll mat[6][6];
};
Matrix zero() {
    Matrix zero;
    for(int i = 0;i < 6;i++) {
        for(int j = 0;j < 6;j++) zero.mat[i][j] = 0;
    }
    return zero;
}
Matrix init() {
    Matrix a = zero();
    for(int i = 0;i < 6;i++) {
        if(i != 6) a.mat[i][i + 1] = 1;
        a.mat[5][i] = 1;
    }
    return a;
}
Matrix I() {
    Matrix I = zero();
    for(int i = 0;i < 6;i++) I.mat[i][i] = 1;
    return I;
}
Matrix matrix_muti(Matrix x, Matrix y, ll p) {
    Matrix c;
    for(int i = 0;i < 6;i++) {
        for(int j = 0;j < 6;j++) {
            c.mat[i][j] = 0;
            for(int k = 0;k < 6;k++) {
                c.mat[i][j] = (c.mat[i][j] + x.mat[i][k] * y.mat[k][j]) % p;
            }
        }
    }
    return c;
}
Matrix fp(Matrix x, ll y, ll p) {
    Matrix ans = I();
    Matrix base = x;
    while(y) {
        if(y & 1) {
            ans = matrix_muti(base, ans, MOD);
        }
        y = y >> 1;
        base = matrix_muti(base, base, MOD);
    }
    return ans;
}
ll solve(int n) {
    Matrix res = fp(init(), n - 1, MOD);
    ll v[6] = {1, 2, 4, 8, 16, 32};
    ll ans = 0;
    for(int i = 0;i < 6;i++) {
        ans = (ans + res.mat[0][i] * v[i]) % MOD;
    }
    return ans;
}