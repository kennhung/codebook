void solve() {
    ll ans = 0;
    for(int i = 0;i < n;i++) {
        ll t = planet[i].first, d = planet[i].second;
        ll Ti = T / t;
        ll x,y,g;
        tie(x,y,g) = exgcd(Ti,t); // x = RevTi
        while(x <= 0) x = x + t;
        // ans += d * (x * Ti)
        // ans % = T
        ans = (ans + muti(muti(x,Ti,T),d,T)) % T;
    }
    printf("%lld",ans);
}
int main() {
    T = 1;
    for(int i = 0;i < n;i++) {
        ll t,d;
        scanf("%lld%lld",&t,&d);
        // ans % t = d
        planet.push_back({t,d});
        T = T*t;
    }
    solve();
}
