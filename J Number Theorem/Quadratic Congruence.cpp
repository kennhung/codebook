ll w, a;
qr qr_muti(qr u, qr v, ll p) {
    qr res;
    res.r = (u.r * v.r % p + (u.q * v.q) % p * w) % p;
    res.q = (u.r * v.q % p + u.q * v.r) % p;
    return res;
}
qr qr_fp(qr x, ll y, ll p) {
    qr res;
    res.r = 1, res.q = 0;
    while(y) {
        if(y & 1) res = qr_muti(res, x, p);
        y >>= 1;
        x = qr_muti(x, x, p);
    }
    return res;
}
ll Led(ll n)
{
    return fp(n, (p - 1) / 2, p);
}
main() {
    do {
        a = rand();
        w = a * a - x;
        w %= p;
        while(w < 0) w += p;
    } while (Led(w) != p - 1);

    qr base;
    base.r = a, base.q = 1;
    qr ans = qr_fp(base, (p + 1) / 2, p);
    ans.r %= p;
    while(ans.r < 0) ans.r += p;
    // ans.r is the solution of n^2=x(mod p)
}
