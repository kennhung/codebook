map<ll, int> fac;
ll muti(ll x, ll y, ll p) {
    ll res = 0;
    while(y) {
        if(y & 1) res = (res + x) % p;
        y >>= 1;
        x = x * 2 % p;
    }
    return res;
}
ll fp(ll x, ll y, ll p) {
    ll res = 1;
    while(y) {
        if(y & 1) res = muti(res, x, p);
        y >>= 1;
        x = muti(x, x, p);
    }
    return res;
}
void cal_rd(ll n, ll &r, ll &d) {
    r = 0;
    while(n % 2 == 0) {
        r++;
        n = n / 2;
    }
    d = n;
}
default_random_engine gen(time(NULL));
uniform_int_distribution<ll> unif(0, (ll)1e18);
bool is_prime(ll n) {
    if(n <= 1) return 0;
    if(n == 2 || n == 3) return 1;

    int times = 10;
    ll r, d;
    cal_rd(n - 1, r, d);
    while(times--) {
        ll a = unif(gen) % (n - 3) + 2;
        ll x = fp(a, d, n);
        if(x == 1 || x == n - 1)
            continue;

        bool probaly_prime = 0;
        for(int i = 0;i < r - 1;i++) {
            x = muti(x, x, n);
            if(x == n - 1) {
                probaly_prime = 1;
                break;
            }
        }
        
        if(probaly_prime)
            continue;
        return 0;
    }
    return 1;
}
ll gcd(ll x, ll y) {
    if(y == 0) return x;
    return gcd(y, x % y);
}
ll g(ll x, ll rnd, ll p) {
    return (rnd * muti(x, x, p) + 1) % p;
}
void pollard_rho(ll n) {
    if(!is_prime(n) && n > 1) {
        ll d;
        while(1) {
            ll x = unif(gen) % n;
            ll rnd = unif(gen) % 1000;
            ll y = g(x, rnd, n);
            d = 1;
            while(d == 1 && x != y) {
                x = g(x, rnd, n);
                y = g(g(y, rnd, n), rnd, n);
                d = gcd(abs(x - y), n);
            }

            if(d == n) continue;
            
            break;
        }
        pollard_rho(d);
        pollard_rho(n / d);
    } else if(n > 1)
        fac[n]++;
}
int main() {
    int t; scanf("%d", &t);
    while(t--) {
        ll n;
        scanf("%lld", &n);
        fac.clear();
        pollard_rho(n);
        for(auto i : fac)
            printf("%lld %d ", i.first, i.second);
        printf("\n");
    }
    return 0;
}