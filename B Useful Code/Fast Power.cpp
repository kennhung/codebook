ll fp(ll x, ll y, ll p) {
    ll ans = 1;
    while(y) {
        if(y & 1) ans = ans * x % p;
        y >>= 1;
        x = x * x % p;
    }
    return ans;
}