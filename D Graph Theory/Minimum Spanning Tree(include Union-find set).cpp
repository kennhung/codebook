struct Edge {
    int a, b;
    ll w;
}edge[2 * MAX_N];
int p[MAX_N + 5], sz[MAX_N + 5];
int find(int x) {
    if(p[x] == x) return x;
    return p[x] = find(p[x]);
}
void unite(int a,int b) {
    a = find(a); b = find(b);
    if(a == b) return;
    if(sz[a] > sz[b]) swap(a, b);
    p[a] = p[b];
    sz[b] += sz[a];
}
bool cmp(Edge a,Edge b) {
    return a.w < b.w;
}
int n, m;
ll MST() {
    int idx = 0;
    ll ans = 0;
    for(int i = 0;i < n - 1;i++) {
        while(idx < m && find(edge[idx].a) == find(edge[idx].b)) {
            idx++;
        }
        if(idx == m) {
            return 0;
        }
        unite(edge[idx].a,edge[idx].b);
        ans = ans + edge[idx].w;
    }
    return ans;
}
void init() {
    for(int i = 1;i <= MAX_N;i++) {
        sz[i] = 1; p[i] = i;
    }
}
void init_input() {
    for(int i = 0;i < m;i++) {
        int a, b; ll w;
        scanf("%d%d%lld",&a, &b, &w);
        edge[i] = {a, b, w};
    }
    sort(edge, edge + m, cmp);
}