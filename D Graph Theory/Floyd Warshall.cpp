void init() {
    memset(dis, INF, sizeof(dis)); // INF can be 0x3f3f3f3f3f3f3f3f
    for(int i = 0;i < m;i++) {
        ll a,b,w; scanf("%lld%lld%lld",&a, &b, &w);
        dis[a][b] = min(dis[a][b], w);
        dis[b][a] = min(dis[b][a], w);
    }
    for(int i = 0;i <= n;i++) dis[i][i] = 0;
}
void Floyd_Warshell() {
    for(int k = 1;k <= n;k++) {
        for(int i = 1;i <= n;i++) {
            for(int j = 1;j <= n;j++) {
                dis[i][j] = min(dis[i][j], dis[i][k] + dis[k][j]);
            }
        }
    }
    // dis[a][b] is the shortest distance between a and b
}