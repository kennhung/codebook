int number(int exp, char is_good) {
    if(is_good == '+') return exp;
    if(is_good == '-') return exp + n;
}
int not_exp(int exp) {
    if(exp <= n) return exp + n;
    if(exp > n) return exp - n;
}
void RevDFS(int x) {
    vis[x] = 1;
    for(auto v : RevAdj[x]) {
        if(!vis[v]) RevDFS(v);
    }
    ord.push_back(x);
}
void DFS(int x, int id) {
    vis[x] = 1;
    SCC_id[x] = id;
    for(auto v : adj[x]) {
        if(!vis[v]) DFS(v, id);
    }
}
void Kosaraju() {
    for(int i = 1;i <= 2 * n;i++) {
        if(!vis[i]) RevDFS(i);
    }
    memset(vis, 0, sizeof(vis));
    int comp = 1;
    for(int i = 2 * n - 1;i >= 0;i--) {
        int x = ord[i];
        if(!vis[x]) DFS(x, comp++);
    }
}
void add_edge(int a, int b) {
    adj[a].push_back(b);
    RevAdj[b].push_back(a);
}
int main() {
    for(int i = 0;i < m;i++) {
        int topping1, topping2; char is_good1, is_good2;
        scanf(" %c %d %c %d", &is_good1, &topping1, &is_good2, &topping2);
        if(topping1 == topping2) {
            int num = number(topping1, is_good1);
            if(is_good1 == is_good2) {
                add_edge(not_exp(num), num);
            }
        } else {
            int num1 = number(topping1, is_good1), num2 = number(topping2, is_good2);
            add_edge(not_exp(num1), num2), add_edge(not_exp(num2), num1);
        }
    }
    Kosaraju();
    for(int i = 1;i <= n;i++) {
        int num_good = number(i, '+'), num_bad = number(i, '-');
        if(SCC_id[num_good] == SCC_id[num_bad]) {
            is_able = 0;
        } else if(SCC_id[num_good] < SCC_id[num_bad]) {
            ans[i] = '+';
        } else if(SCC_id[num_good] > SCC_id[num_bad]) {
            ans[i] = '-';
        }
    }
}