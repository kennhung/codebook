int match[MAX_N]; bool used[MAX_N]; vector<int> adj[MAX_N];
bool DFS(int x) {
    for(auto u : adj[x]) {
        if(used[u]) continue;
        used[u] = 1;
        int next = match[u];
        if(next == -1 || DFS(next)) {
            match[u] = x;
            return 1;
        }
    }
    return 0;
}
int n, m, k; // n = left set size, m = right set size, k = number of edge
int Bipartite_match() {
    memset(match, -1, sizeof(match));
    int match_number = 0;
    for(int i = 1;i <= n;i++) {
        memset(used, 0, sizeof(used));
        match_number += DFS(i);
    }
    // and if(match[i] != -1) {i(right set) and match[i](left set) match}
    return match_number;
}
void init() {
    for(int i = 0;i < k;i++) {
        int a, b; scanf("%d%d", &a, &b);
        adj[a].push_back(b); // a in left set, b in right set
    }
}
