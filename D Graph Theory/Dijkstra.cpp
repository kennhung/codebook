priority_queue <pii, vector<pii>, greater<pii>> dij;
dis[1] = 0, dij.push({0,1});
while(!dij.empty()) {
    pii top = dij.top(), dij.pop();
    int a = top.second;
    if(vis[a]) continue;
    vis[a] = 1;
    for(auto i : adj[a]) {
        ll b = i.first, w = i.second;
        if(dis[b] > dis[a] + w) {
            dis[b] = dis[a] + w;
            dij.push({dis[b],b});
        }
    }
}