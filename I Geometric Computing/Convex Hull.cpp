struct Point {
    ll x, y;
    int i;
};
vector <Point> point, upper, lower;
ll cross(Point a, Point b, Point c) {
    P v1 = {b.x - a.x, b.y - a.y};
    P v2 = {c.x - a.x, c.y - a.y};
    return (conj(v1) * v2).Y;
}
bool cmp(Point a, Point b) {
    if(a.x != b.x) return a.x < b.x; 
    return a.y < b.y;
}
int n;
void init() {
    scanf("%d", &n);
    for(int i = 0;i < n;i++) {
        ll x, y; scanf("%lld%lld", &x, &y);
        point.push_back({x, y, i + 1}); // (x, y) and number
    }
    sort(point.begin(), point.end(), cmp);
}
void Convex_Hull() {
    for(int i = 0;i < n;i++) {
        int sz;
        while((sz = upper.size()) > 1 && cross(upper[sz - 2], point[i], upper[sz - 1]) < 0)
            upper.pop_back();
        upper.push_back(point[i]);
    }

    for(int i = n - 1;i >= 0;i--) {
        int sz;
        while((sz = lower.size()) > 1 && cross(lower[sz - 2], point[i], lower[sz - 1]) < 0)
            lower.pop_back();
        lower.push_back(point[i]);
    }
    // the convex hull order : upper[0 ~ upper.size() - 2] + lower[0 ~ lower.size() - 1]; (the upper[0] is equal with lower.back)
}
int main() {
    init(); Convex_Hull();
    printf("%d\n", (int)upper.size() + (int)lower.size() - 2);
    for(int i = 0;i < (int)upper.size() - 1;i++)
        printf("%lld %lld\n",upper[i].x, upper[i].y);
    for(int i = 0;i < (int)lower.size() - 1;i++)
        printf("%lld %lld\n",lower[i].x, lower[i].y);
}