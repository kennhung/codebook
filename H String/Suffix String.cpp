vector<int> p(400005), c(400005);
int cnt[400005], pos[400005];
vector<pair<pair<int, int>, int>> a_new(400005);
void radix_sort(vector<pair<pair<int, int>, int>> &a) {
    int n = (int)a.size(); {
        memset(cnt, 0, sizeof(cnt));
        for(auto x : a) cnt[x.first.second]++;

        pos[0] = 0;
        for(int i = 1;i < n;i++) pos[i] = pos[i - 1] + cnt[i - 1];

        for(auto x : a) {
            int i = x.first.second;
            a_new[pos[i]] = x;
            pos[i]++;
        }
        for(int i = 0;i < n;i++) a[i] = a_new[i];
    }

    {
        memset(cnt, 0, sizeof(cnt));
        for(auto x : a)  cnt[x.first.first]++

        pos[0] = 0;
        for(int i = 1;i < n;i++) pos[i] = pos[i - 1] + cnt[i - 1];
        
        for(auto x : a) {
            int i = x.first.first;
            a_new[pos[i]] = x;
            pos[i]++;
        }
        for(int i = 0;i < n;i++) a[i] = a_new[i];
    }
}
int main() {
    string s; cin >> s;
    s += "$";
    int n = s.size(); 
    {
        vector<pair<char, int>> a(n);
        for(int i = 0;i < n;i++) a[i] = {s[i], i};
        sort(a.begin(), a.end());

        for(int i = 0;i < n;i++) p[i] = a[i].second;

        c[p[0]] = 0;
        for(int i = 1;i < n;i++) {
            if(a[i].first == a[i - 1].first) c[p[i]] = c[p[i - 1]];
            else c[p[i]] = c[p[i - 1]] + 1;
        }
    }
    int k = 0;
    while((1 << k) <= n) {
        vector<pair<pair<int, int>, int>> a(n);
        for(int i = 0;i < n;i++) a[i] = {{c[i], c[(i + (1 << k)) % n]}, i}; // % n超重要
        radix_sort(a);

        for(int i = 0;i < n;i++) p[i] = a[i].second;

        c[p[0]] = 0;
        for(int i = 1;i < n;i++) {
            if(a[i].first == a[i - 1].first) c[p[i]] = c[p[i - 1]];
            else c[p[i]] = c[p[i - 1]] + 1;
        }
        k++;
    }

    for(int i = 0;i < n;i++) cout << p[i] << " " ;//<< s.substr(p[i], n - p[i]) << endl;
}