void fail() {
    f[0] = 0;
    int j = 0;
    for(int i = 1;i < (int)pat.size();i++) {
        while(j != 0 && pat[i] != pat[j])  j = f[j - 1];
 
        if(pat[i] == pat[j]) j++;

        f[i] = j;
    }
}
int matching() {
    int res = 0, j = 0;
    for(int i = 0;i < (int)text.size();i++) {
        while(j != 0 && text[i] != pat[j])  j = f[j - 1];
 
        if(text[i] == pat[j]) {
            if(j == (int)pat.size() - 1) { // find a match
                res++; 
                j = f[j];
            } else {
                j++;
            }
        }
    }
    return res;
}