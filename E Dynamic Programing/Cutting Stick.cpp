int solve(int l, int r) {
    if(r - l <= 1) return 0;
    if(dp[l][r]) return dp[l][r];
    int ans = INF, len = cut[r] - cut[l]; 
    for(int i = l + 1;i < r;i++) {
        ans = min(ans, len + solve(l, i) + solve(i, r));
    }
    return dp[l][r] = ans;
}
int main() {
    cut[0] = 0, cut[n + 1] = l; // and input cut[1 ~ n]
    printf("%d", solve(0, n + 1));
}