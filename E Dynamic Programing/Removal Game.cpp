ll solve(ll l, ll r) { //calculate left ~ right interval, the first player can get how many score
    ll ans1,ans2;
    if(l > r) return 0;
    if(l == r) return arr[l];
    if(dp[l][r]) return dp[l][r];
    if(l <= r + 2) ans1 = min(arr[l] + solve(l + 2,r),arr[l] + solve(l + 1,r - 1));
    if(l <= r + 2) ans2 = min(arr[r] + solve(l + 1,r - 1),arr[r] + solve(l,r - 2));
    return dp[l][r] = max(ans1,ans2);
}