map<ll, int> subset_sum_cnt;
ll arr[45], x;
int n;
void DFS1(int level, ll sum) {
    if(sum > x) return;
    if(level == n / 2) {
        subset_sum_cnt[sum]++;
        // sum_list2.push_back(sum);
        return;
    }
    DFS1(level + 1, sum + arr[level]);
    DFS1(level + 1, sum);
}
vector<int> sum_list;
void DFS2(int level, ll sum) {
    if(sum > x) return;
    if(level == n) {
        sum_list.push_back(sum);
        return;
    }
    DFS2(level + 1, sum + arr[level]);
    DFS2(level + 1, sum);
}
// first input n, x (the target sum of subset) and arr[0 ~ n - 1]
ll solve() {
    DFS1(0, 0);
    DFS2(n / 2, 0);
    ll ans = 0;
    for(auto s : sum_list) {
        ans += subset_sum_cnt[x - s];
        /* if the target sum is "smaller than and equal x", 
        ll dif = x - ans2[i];
        ll pos = upper_bound(sum_list2.begin(), sum_list2.end(), dif) - sum_list2.begin();
        ans = ans + pos;
        */
    }
    return ans;
}