ll muti(ll x,ll y,ll p) { // x * y % p
    vector <ll> dec;
    while(x > 0) {
        ll d = x % 10;
        x = x / 10;
        dec.push_back(d);
    }
    ll ans = 0;
    for(int i = dec.size() - 1;i >= 0;i--) {
        ans = (ans * 10 + y * dec[i]) % p;
    }
    return ans;
}