ll a,b,p;
map<ll, ll> power;
vector <pair<ll, ll>> fac;
void factorize();
ll phi();
ll fp(ll x, ll y, ll p) {
    if(y == 0) return 1;
    if(y % 2) return fp(x,y - 1,p) * x % p;
    ll ans = fp(x,y/2,p);
    return ans * ans % p;
}
void solve1() {
    ll m = ceil(sqrt(p));
    ll l = b;
    power[l] = 1;
    for(ll i = 1;i < m;i++) {
        l = l * a % p;
        power[l] = i + 1;
    }
    ll k = fp(a,m,p);
    ll res = k;
    for(ll i = 1;i <= m;i++) {
        if(power[res]) {
            ll ans = i * m - power[res] + 1;
            cout << ans << " ";
            power.clear();
            return;
        }
        res = res * k % p;
    }
    power.clear();
    cout << "-1 ";
}
ll solve2() {
    ll m = ceil(sqrt(p));
    ll l = b;
    power[l] = 1;
    for(ll i = 1;i < m;i++) {
        l = l * a % p;
        power[l] = i + 1;
    }
    ll k = fp(a,m,p);
    ll res = k;
    for(ll i = 1;i <= m;i++) {
        if(power[res]) {
            ll ans = i * m - power[res] + 1;
            power.clear();
            return ans;
        }
        res = res * k % p;
    }
    return 87;
}
void init() {
    fac.clear();
    power.clear();
}
int main() {
    int t;
    cin >> t;
    while(t--) {
        init();
        cin >> a >> b >> p;
        solve1();
        factorize();
        b = 1;
        ll d = solve2();
        if(d == phi()) cout <<"Yes\n";
        else cout << "No\n";
    }
    return 0;
}
void factorize()
{
    ll n = p;
    for(ll i = 2;i * i <= n;i++)
    {
        if(n % i == 0) fac.push_back({i,0});
        while(n % i == 0)
        {
            fac.back().second++;
            n = n / i;
        }
    }
    if(n > 1) fac.push_back({n,1});
}
ll phi()
{
    ll ans = 1;
    for(auto u : fac)
    {
        ll prime = u.first;
        ll prime_power = u.second;
        ans = ans * (prime - 1) * fp(prime,prime_power - 1,p);
    }
    return ans;
}