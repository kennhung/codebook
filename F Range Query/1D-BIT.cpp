int n; ll arr[200005], pre[200005], BIT[200005];
void upd(ll x,ll dif) { // update the value at position x by difference dif
    int k = x;
    while(k <= n) {
        BIT[k] = BIT[k] + dif;
        k += k & -k;
    }
}
ll sum(ll x) { // return the sum of values in range [1, x]
    ll ans = 0, k = x;
    while(k > 0) {
        ans = ans + BIT[k];
        k -= k & -k;
    }
    return ans;
}
void build() {
    for(int i = 1;i <= n;i++) { // build, index must start at 1
        scanf("%lld", &arr[i]);
        pre[i] = pre[i - 1] + arr[i];
        int k = i & -i;
        BIT[i] = pre[i] - pre[i - k];
    }
}