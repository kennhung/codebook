vector<pair<ll, ll>> adj[MAX_N]; // (neighbor, weight)
bool vis[MAX_N];
int point;
void DFS(int x, ll dis, ll &mx) {
    if(dis > mx) {
        mx = dis;
        point = x;
    }
    vis[x] = 1;
    for(auto v : adj[x]) {
        if(!vis[v.first]) {
            DFS(v.first, dis + v.second, mx);
        }
    }
}
ll Tree_Diameter() {
    ll mx = 0;
    DFS(1, 0, mx);
    memset(vis, 0, sizeof(vis));
    DFS(point, 0, mx);
    return mx;
}